--[[
	Minetest mod 3d_torch
	code by Semmett9 / Infinitum 
]]

local def = {
	tiles = {{
		name = "default_torch_on_floor_animated.png",
		animation = {
			type = "vertical_frames", 
			aspect_w = 32, 
			aspect_h = 32, 
			length = 1 -- length is used as speed. smaller = faster
		}
	}}
}

minetest.override_item("default:torch", def)
minetest.override_item("default:torch_wall", def)
minetest.override_item("default:torch_ceiling", def)

local rotation = {
	[0] = 3,
	[1] = 4,
	[2] = 2,
	[3] = 5
}

minetest.register_lbm({
	name = "3d_torch:3d_torch1",
	nodenames = {"3d_torch:torch_floor", "3d_torch:torch_wall", "3d_torch:torch_ceiling"},
	action = function(pos, node)
		if node.name == "3d_torch:torch_floor" then
			node.param2 = 1;
			node.name = "default:torch";
			minetest.set_node(pos, node)
		elseif node.name == "3d_torch:torch_ceiling" then
			minetest.set_node(pos, {name = "default:torch_ceiling"})
		elseif node.name == "3d_torch:torch_wall" then
			node.param2 = rotation[node.param2] or 0 -- 0 in case something is wrong
			node.name = "default:torch_wall" 
			minetest.set_node(pos, node)
		end
	end
})
